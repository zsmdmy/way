/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"gitee.com/zsmdmy/way/code"
	"github.com/spf13/cobra"
)

var (
	connection string
	table      string
	directory  string
	force      bool
)

var genModel = &cobra.Command{
	Use:   "gen_model",
	Short: "根据数据表生成结构体的工具",
	Long:  `根据数据表生成结构体的工具`,
	Run: func(cmd *cobra.Command, args []string) {
		g := code.NewGormModelGenerator(connection)
		g.Gen(force, table)
		//generator := dbext.NewModelGenerator(connection, directory)
		//generator.GenModel(force, table)
	},
	Args: func(cmd *cobra.Command, args []string) error {
		return nil
	},
}

func init() {
	RootCmd.AddCommand(genModel)
	genModel.Flags().StringVarP(&connection, "connection", "c", "default", "connection (数据库配置名称)")
	genModel.Flags().StringVarP(&table, "table", "t", "", "table (数据表名称)")
	genModel.Flags().StringVarP(&directory, "directory", "d", "model", "directory (结构体文件存放目录)")
	genModel.Flags().BoolVarP(&force, "force", "f", false, "force (是否覆盖 默认false)")
}
