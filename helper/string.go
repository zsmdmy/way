package helper

import (
	"math/rand"
	"regexp"
	"strings"
	"time"
)

// StringToCamelCase 蛇形字符串转大驼峰
func StringToCamelCase(snakeStr string) string {
	data := make([]byte, 0, len(snakeStr))
	j := false
	k := false
	num := len(snakeStr) - 1
	for i := 0; i <= num; i++ {
		d := snakeStr[i]
		if k == false && d >= 'A' && d <= 'Z' {
			k = true
		}
		if d >= 'a' && d <= 'z' && (j || k == false) {
			d = d - 32
			j = false
			k = true
		}
		if k && d == '_' && num > i && snakeStr[i+1] >= 'a' && snakeStr[i+1] <= 'z' {
			j = true
			continue
		}
		data = append(data, d)
	}
	return string(data[:])
}

// StringToSnakeCase 大驼峰转蛇形
func StringToSnakeCase(camelStr string) string {
	data := make([]byte, 0, len(camelStr)*2)
	j := false
	num := len(camelStr)
	for i := 0; i < num; i++ {
		d := camelStr[i]
		// or通过ASCII码进行大小写的转化
		// 65-90（A-Z），97-122（a-z）
		//判断如果字母为大写的A-Z就在前面拼接一个_
		if i > 0 && d >= 'A' && d <= 'Z' && j {
			data = append(data, '_')
		}
		if d != '_' {
			j = true
		}
		data = append(data, d)
	}
	//ToLower把大写字母统一转小写
	return strings.ToLower(string(data[:]))
}

// StringFilterEmoji 过滤emoji
func StringFilterEmoji(str string) string {
	re := regexp.MustCompile("[\\x{1F600}-\\x{1F64F}\\x{1F300}-\\x{1F5FF}\\x{1F680}-\\x{1F6FF}\\x{2600}-\\x{26FF}\\x{2700}-\\x{27BF}\\x{1F900}-\\x{1F9FF}\\x{1F1E0}-\\x{1F1FF}]")
	filteredStr := re.ReplaceAllString(str, "")
	return filteredStr
}

// StringToInitialsAbbreviation 提取单词首字母
func StringToInitialsAbbreviation(s string) string {
	re := regexp.MustCompile(`[A-Z][a-z]*`)
	words := re.FindAllString(s, -1)
	var initials []byte

	for _, word := range words {
		if len(word) > 0 {
			initials = append(initials, word[0]) // 获取单词的首字母
		}
	}

	return strings.ToLower(string(initials))
}

func StringRandom(length int) string {
	var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")

	rand.NewSource(time.Now().UnixNano()) // 初始化随机种子
	b := make([]rune, length)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}
