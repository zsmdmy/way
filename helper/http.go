package helper

import (
	"encoding/json"
	"github.com/pkg/errors"
	"io"
	"net/http"
)

func HttpParseBody(response *http.Response, result interface{}) error {
	body, err := io.ReadAll(response.Body)
	if err == nil {
		err = json.Unmarshal(body, &result)
		return errors.WithMessagef(err, "解析body失败 %s", string(body))
	}
	return nil
}

func HttpParseBodyWithResponseString(response *http.Response, result interface{}) (error, string) {
	body, err := io.ReadAll(response.Body)
	if err == nil {
		err = json.Unmarshal(body, &result)
		return errors.WithMessagef(err, "解析body失败 %s", string(body)), string(body)
	}
	return nil, string(body)
}
