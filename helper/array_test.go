package helper

import (
	"fmt"
	"reflect"
	"testing"
)

func TestArrayKeys(t *testing.T) {
	type args struct {
		arr []interface{}
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			name: "test1",
			args: args{
				arr: []interface{}{1, 2, 3, 4},
			},
			want: []int{0, 1, 2, 3},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ArrayKeys(tt.args.arr); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ArrayKeys() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestArrayMapKeyColumn(t *testing.T) {
	type args struct {
		arr []map[interface{}]interface{}
		key interface{}
		out []interface{}
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "[]map[string]string",
			args: args{
				arr: []map[interface{}]interface{}{
					{"key1": "value1-1", "key2": "value1-2"},
					{"key1": "value2-1", "key2": "value2-2"},
					{"key1": "value3-1", "key2": "value3-2"},
				},
				key: "key1",
				out: []interface{}{},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ArrayMapKeyColumn(tt.args.arr, tt.args.key, &tt.args.out)
			fmt.Println(tt.args.out)
		})
	}
}
