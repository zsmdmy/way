package helper

func ArrayKeys(arr []interface{}) []int {
	var keys []int
	for key, _ := range arr {
		keys = append(keys, key)
	}
	return keys
}

func ArrayMapKeyColumn(arr []map[interface{}]interface{}, key interface{}, out *[]interface{}) {
	for _, item := range arr {
		*out = append(*out, item[key])
	}
}
