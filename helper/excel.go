package helper

import (
	"math"
	"strconv"
	"time"
)

// 将excel时间日期格式转化为标准时间
func ExcelColToDateTime(str string) string {

	layout := "2006-01-02 15:04:05"

	if location, err := time.ParseInLocation(layout, str, time.Local); err == nil {
		retStr := location.Format(layout)
		return retStr
	}

	if days, err := strconv.ParseFloat(str, 64); err == nil && days > 10000 {
		excelTime := time.Unix(-2209190400, 0)
		timestamp := excelTime.Add(time.Second * time.Duration(math.Round(days*86400)))
		retStr := time.Unix(timestamp.Unix(), 0).Format(layout)
		return retStr
	}

	return str
}
