package helper

import (
	"github.com/go-redsync/redsync/v4"
	"github.com/go-redsync/redsync/v4/redis/goredis/v9"
	"github.com/redis/go-redis/v9"
)

func Lock(lockKey string, redis *redis.Client, options ...redsync.Option) (*redsync.Mutex, error) {
	pool := goredis.NewPool(redis)

	rs := redsync.New(pool)

	mutex := rs.NewMutex(lockKey, options...)

	return mutex, mutex.Lock()
}
