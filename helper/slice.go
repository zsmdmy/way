package helper

import "github.com/pkg/errors"

func SliceUnique(originals interface{}) (interface{}, error) {

	switch slice := originals.(type) {
	case []string:
		result := []string{}
		tmp := map[string]byte{}
		for _, original := range slice {
			if _, ok := tmp[original]; !ok {
				tmp[original] = 0
				result = append(result, original)
			}
		}
		return result, nil
	case []int64:
		result := []int64{}
		tmp := map[int64]byte{}
		for _, original := range slice {
			if _, ok := tmp[original]; !ok {
				tmp[original] = 0
				result = append(result, original)
			}
		}
		return result, nil
	case []int32:
		result := []int32{}
		tmp := map[int32]byte{}
		for _, original := range slice {
			if _, ok := tmp[original]; !ok {
				tmp[original] = 0
				result = append(result, original)
			}
		}
		return result, nil
	default:
		return nil, errors.Errorf("Unknown type: %T", slice)
	}
}

func SliceContainsStr(slice []string, item string) bool {
	for _, i := range slice {
		if i == item {
			return true
		}
	}
	return false
}
