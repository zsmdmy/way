package way

import (
	"fmt"
	"github.com/spf13/viper"
)

var v *viper.Viper

func Config() *viper.Viper {
	configFilePath := "./config.yaml"
	if v == nil {
		v = newConfig(configFilePath)
	}
	return v
}

func newConfig(configFilePath string) *viper.Viper {
	v := viper.New()
	v.SetConfigFile(configFilePath)
	err := v.ReadInConfig() // Find and read the config file
	if err != nil {         // Handle errors reading the config file
		panic(fmt.Errorf("fatal error config file: %w", err))
	}
	return v
}
