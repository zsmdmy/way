package dingtalk

import "encoding/json"

type Message interface {
	ToJson(keyword string) ([]byte, error)
}

type Text struct {
	MsgType string `json:"msgtype"`
	At      struct {
		AtMobiles []string `json:"atMobiles,omitempty"`
		AtUserIds []string `json:"atUserIds,omitempty"`
		IsAtAll   bool     `json:"isAtAll,omitempty"`
	} `json:"at,omitempty"`
	Text struct {
		Content string `json:"content"`
	} `json:"text"`
}

func NewText() *Text {
	return &Text{
		MsgType: "text",
	}
}
func (t *Text) Content(content string) *Text {
	t.Text.Content = content
	return t
}

func (t *Text) AtMobiles(mobiles []string) *Text {
	t.At.AtMobiles = mobiles
	return t
}

func (t *Text) AtUserIds(ids []string) *Text {
	t.At.AtUserIds = ids
	return t
}

func (t *Text) IsAtAll(isAtAll bool) *Text {
	t.At.IsAtAll = isAtAll
	return t
}

func (t *Text) ToJson(keyword string) ([]byte, error) {
	t.Text.Content = keyword + t.Text.Content
	bts, err := json.Marshal(t)
	if err != nil {
		return nil, err
	}
	return bts, nil
}

type Link struct {
	MsgType string `json:"msgtype"`
	Link    struct {
		Text       string `json:"text"`
		Title      string `json:"title"`
		PicUrl     string `json:"picUrl"`
		MessageUrl string `json:"messageUrl"`
	} `json:"link"`
}

type Markdown struct {
	MsgType  string `json:"msgtype"`
	Markdown struct {
		Title string `json:"title"`
		Text  string `json:"text"`
	} `json:"markdown"`
	At struct {
		AtMobiles []string `json:"atMobiles"`
		AtUserIds []string `json:"atUserIds"`
		IsAtAll   bool     `json:"isAtAll"`
	} `json:"at"`
}

type ActionCard struct {
	MsgType    string `json:"msgtype"`
	ActionCard struct {
		Title          string `json:"title"`
		Text           string `json:"text"`
		BtnOrientation string `json:"btnOrientation"`
		SingleTitle    string `json:"singleTitle"`
		SingleURL      string `json:"singleURL"`
	} `json:"actionCard"`
}

type FeedCard struct {
	Msgtype  string `json:"msgtype"`
	FeedCard struct {
		Links []struct {
			Title      string `json:"title"`
			MessageURL string `json:"messageURL"`
			PicURL     string `json:"picURL"`
		} `json:"links"`
	} `json:"feedCard"`
}
