package code

import (
	"fmt"
	"gitee.com/zsmdmy/way/helper"
	"gitee.com/zsmdmy/way/util"
	"strings"
)

type Import struct {
	Name string
}

type Imports []Import

// AppendUnique appends only unique imports to the receiver.
func (receiver *Imports) AppendUnique(imports ...Import) {
	// Create a map to track existing imports
	existing := make(map[string]bool)

	// Populate the map with existing imports
	for _, imp := range *receiver {
		existing[imp.Name] = true
	}

	// Append only unique imports
	for _, imp := range imports {
		if !existing[imp.Name] {
			*receiver = append(*receiver, imp)
			existing[imp.Name] = true
		}
	}
}

type Struct struct {
	Name      string
	ShortName string
	Comment   string
}

type Column struct {
	Name     string
	JsonName string
	Type     string
	Comment  string
	Tag      string
}

type ConstField struct {
	Name    string
	Value   interface{}
	Type    string
	Comment string
}

func (self *Column) ToEnum() []ConstField {
	constFields := []ConstField{}
	results := strings.Split(self.Comment, ";")
	if len(results) == 1 {
		return constFields
	}
	for _, result := range results {
		r := strings.Split(result, ":")
		if len(r) == 1 {
			return constFields
		}
		v := r[0]
		k := r[1]

		constField := ConstField{
			Name:    "",
			Value:   v,
			Type:    self.Type,
			Comment: k,
		}

		if constField.Type == "string" {
			constField.Value = fmt.Sprintf("\"%s\"", constField.Value)
		}

		k = util.Translate(k, "zh", "en")
		k = strings.ReplaceAll(k, " ", "_")
		k = helper.StringToCamelCase(k)
		constField.Name = self.Name + k
		constFields = append(constFields, constField)
	}
	return constFields
}

type Function struct {
	Name     string
	Receiver Struct
	Body     []string
	Return   []Param
	Params   []Param
}

type Param struct {
	Name      string
	ShortName string
}
