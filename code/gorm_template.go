package code

var StructTemplate = `
package {{.PackageName}}

import (
{{range $i,$import:=.Imports }}
{{$import.Name}}{{end}}
)

{{$strName := .Struct.Name}}
{{range $i,$constant:=.Constants}}
//{{$constant.Comment}}
const {{$strName}}{{$constant.Name}} {{$constant.Type}} = {{$constant.Value}}
{{end}}

type {{.Struct.Name}} struct {
{{range $i,$column:=.Columns }}
{{$column.Name}} {{$column.Type}} {{$column.Tag}} //{{$column.Comment}}{{end}}
}


{{range $i,$function:=.Functions }}
{{if ne $function.Receiver.Name ""}}
func({{$function.Receiver.ShortName}} *{{$function.Receiver.Name}}){{$function.Name}}{{else}}func {{$function.Name}}{{end}}(
{{- range $i,$param:=$function.Params -}}{{if ne $i 0}},{{end}}{{$param.ShortName}} {{$param.Name}}{{end}})({{range $i,$param:=$function.Return}}{{if ne $i 0}},{{end}}{{$param.ShortName}} {{$param.Name}}{{end}}){
{{range $u,$body := $function.Body -}}
{{$body}}
{{end -}}
}
{{end}}
`
