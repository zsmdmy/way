package way

import (
	"gitee.com/zsmdmy/way/logger"
	"go.uber.org/zap"
	"strings"
	"sync"
)

var logMap sync.Map

func Logger(name ...string) *zap.Logger {
	var loggerName string
	defaultName := "log.default"
	if len(name) > 0 {
		loggerName = "log." + name[0]
	} else {
		loggerName = defaultName
	}
	if _, ok := logMap.Load(loggerName); !ok {
		config := logger.NewConfig()
		switch true {
		case Config().IsSet(loggerName):
			if err := Config().UnmarshalKey(loggerName, &config); err != nil {
				panic(err)
			}
		case Config().IsSet(defaultName):
			if err := Config().UnmarshalKey(defaultName, &config); err != nil {
				panic(err)
			}
		default:

		}
		logMap.Store(loggerName, logger.New(config))
	}
	c, _ := logMap.Load(loggerName)
	if client, ok := c.(*zap.Logger); ok {
		return client
	} else {
		panic("logger 初始化错误")
	}
}

func nameToPath(name string) string {
	path := strings.ReplaceAll(name, ".", "/")
	return "./" + path + "/"
}
