package es

import (
	"fmt"
	"github.com/olivere/elastic/v7"
)

func New(config *Config) *elastic.Client {
	var clinetOptions []elastic.ClientOptionFunc
	clinetOptions = append(clinetOptions,
		elastic.SetSniff(false),
		elastic.SetURL(fmt.Sprintf("http://%s:%s", config.Host, config.Port)),
		elastic.SetBasicAuth(config.Username, config.Password),
	)
	clinetOptions = append(clinetOptions, elastic.SetGzip(config.Gzip)) //启动gzip压缩

	if config.InfoLog {
		clinetOptions = append(clinetOptions, elastic.SetErrorLog(config.Logger)) // 设置错误日志输出
	}
	if config.ErrorLog {
		clinetOptions = append(clinetOptions, elastic.SetInfoLog(config.Logger)) // 设置info日志输出
	}
	if config.TraceLog {
		clinetOptions = append(clinetOptions, elastic.SetTraceLog(config.Logger)) // 设置trace日志输出
	}

	if client, err := elastic.NewClient(clinetOptions...); err != nil {
		panic(err)
	} else {
		return client
	}
}
