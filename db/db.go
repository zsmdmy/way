package db

import (
	"fmt"
	"gorm.io/driver/mysql"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"time"
)

func New(config *Config) *gorm.DB {
	switch config.DbType {
	case "mysql":
		return NewMysql(config)
	case "postgres":
		return NewPostgres(config)
	default:
		return NewMysql(config)
	}
}

func NewMysql(config *Config) *gorm.DB {
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=%s&parseTime=%s&loc=%s",
		config.User, config.Password, config.Host, config.Port, config.Database, config.Charset, config.ParseTime, config.Loc)
	db, err := gorm.Open(mysql.New(mysql.Config{
		DSN:                       dsn,
		DefaultStringSize:         256,   // string 类型字段的默认长度
		DisableDatetimePrecision:  true,  // 禁用 datetime 精度，MySQL 5.6 之前的数据库不支持
		DontSupportRenameIndex:    true,  // 重命名索引时采用删除并新建的方式，MySQL 5.7 之前的数据库和 MariaDB 不支持重命名索引
		DontSupportRenameColumn:   true,  // 用 `change` 重命名列，MySQL 8 之前的数据库和 MariaDB 不支持重命名列
		SkipInitializeWithVersion: false, // 根据当前 MySQL 版本自动配置
	}), &gorm.Config{
		Logger: config.Logger,
	})

	if err != nil {
		panic(err)
	}

	Db, _ := db.DB()
	// SetMaxIdleConns 设置空闲连接池中连接的最大数量
	Db.SetMaxIdleConns(config.MaxIdleConns)
	// SetMaxOpenConns 设置打开数据库连接的最大数量。
	Db.SetMaxOpenConns(config.MaxOpenConns)
	// SetConnMaxLifetime 设置了连接可复用的最大时间。
	Db.SetConnMaxIdleTime(time.Minute * time.Duration(config.ConnMaxIdleMinutes))
	Db.SetConnMaxLifetime(time.Minute * time.Duration(config.ConnMaxLifeMinutes))

	return db
}

func NewPostgres(config *Config) *gorm.DB {

	dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=disable",
		config.Host, config.User, config.Password, config.Database, config.Port)

	db, err := gorm.Open(postgres.New(postgres.Config{
		DSN: dsn,
	}), &gorm.Config{
		Logger: config.Logger,
	})

	if err != nil {
		panic(err)
	}

	Db, _ := db.DB()
	// SetMaxIdleConns 设置空闲连接池中连接的最大数量
	Db.SetMaxIdleConns(config.MaxIdleConns)
	// SetMaxOpenConns 设置打开数据库连接的最大数量。
	Db.SetMaxOpenConns(config.MaxOpenConns)
	// SetConnMaxLifetime 设置了连接可复用的最大时间。
	Db.SetConnMaxIdleTime(time.Minute * time.Duration(config.ConnMaxIdleMinutes))
	Db.SetConnMaxLifetime(time.Minute * time.Duration(config.ConnMaxLifeMinutes))

	return db
}
