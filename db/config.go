package db

import "gorm.io/gorm/logger"

type Config struct {
	Host               string
	Port               string
	User               string
	Password           string
	Database           string
	Charset            string
	MaxIdleConns       int    `mapstructure:"max_idle_conns,omitempty"`
	MaxOpenConns       int    `mapstructure:"max_open_conns,omitempty"`
	ConnMaxIdleMinutes int64  `mapstructure:"conn_max_idle_minutes,omitempty"`
	ConnMaxLifeMinutes int64  `mapstructure:"conn_max_life_minutes,omitempty"`
	ParseTime          string `mapstructure:"parse_time" json:"parse_time,omitempty"`
	Loc                string `json:"loc,omitempty"`
	Logger             logger.Interface
	DbType             string `mapstructure:"db_type" json:"db_type,omitempty"`
	SlowThreshold      int64  `mapstructure:"slow_threshold" json:"slow_threshold,omitempty"`
}

func NewConfig() *Config {
	return &Config{
		//ParseTime:          "true",
		//Loc:                "Local",
		//Charset:            "utf8mb4",
		//MaxIdleConns:       100,
		//MaxOpenConns:       100,
		//ConnMaxIdleMinutes: 5,
		//ConnMaxLifeMinutes: 60,
	}
}
