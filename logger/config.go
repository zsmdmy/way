package logger

type Config struct {
	Level      string
	Stdout     bool
	Filepath   string
	MaxSize    int `mapstructure:"max_size"`
	MaxBackups int `mapstructure:"max_backups"`
	MaxAge     int `mapstructure:"max_age"`
	Compress   bool
	Localtime  bool
}

func NewConfig() *Config {
	return &Config{
		Level:      "info",
		Stdout:     true,
		Filepath:   "./logs/default",
		MaxSize:    2,
		MaxBackups: 7,
		MaxAge:     7,
		Compress:   true,
		Localtime:  true,
	}
}
